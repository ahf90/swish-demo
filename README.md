![](swish-demo-flow.png)

# Instructions

1. Add your API Key and the API URL to 1/secrets.yaml
2. Run `kubectl apply -f . -R`


# Parts

## Producer
[Repo](https://gitlab.com/ahf90/read-api-write-redis)


The producer is a containerized python application that queries the Swish API and stores the returned data into redis.
It's deployed as a k8s CRON job, which runs the application minutely.
Using this prebuilt abstraction has numerous advantages:

1. Less code means there is less room for bugs and human errors
2. Prebuilt, widely-used tools are more stable than custom-built tools
3. Simplified scaling.  If the app requirements change, and there is a situation where we need multiple producers, we can leverage k8s to scale for us.
4. Reliability.  If the container crashes or fails, you can configure the job to retry.

I picked Python because it's readable, lightweight, observable, and starts up quickly. 
Go & Rust are other good choices for microservices for the same reason.  There's no right answer here.

## Redis
Honestly, Kafka & RabbitMQ would have also been fine choices.  I chose Redis for the message broker because it:   
1. Delivers data quickly because it stores data in memory. This is important for processing live sports data. 
2. Scales both on the producer/consumer side
3. Is easy to set up and has less overhead than Kafka.

There's two downsides to Redis, both due to its in-memory data store
1.  It can't handle datasets larger than the datastore's memory
2.  Messages don't persist--they're gone from memory once they've been pulled off the queue.

For the first point, sports API datasets are not singularly large and can easily be broken up.

For #2, instead of trying to recover a lost message, which you could do in Kafka, it would make more sense just to requery the API.  

In fact, with constantly-changing data like this, it may be dangerous to repull an old message because it is likely to be stale.

## Consumer
[Repo](https://gitlab.com/ahf90/read-redis-write-pg)

The consumer is a containerized python application that polls Redis and stores the returned data into PostgreSQL.
It's deployed as a "Deployment" abstraction using k8s.  We could scale easily by increasing the number of replicas.
If the consumer crashes, k8s will ensure that a healthy pod is launched.

The language choice reasoning is the same here as it is for the producer.

## PostgreSQL
The only reason I could think of the use PostgreSQL over another SQL DB is that it handles concurrency and locking better.
If the app scaled, and we were constantly updating data from the same tables, Postgres would be a better choice.
Otherwise, it's not really possible to make an informed decision unless we know the use case of the data.

### A case for NoSQL
When pulling external data, NoSQL datastores are less brittle.  
If the data provider changes their schema in any way, your SQL datastore is immediately broken.
NoSQL also scales better due to its sharding capabilities. You *can* shard in SQL, but it's not nearly as easy.
It's also easier to handle the atomicity and concurrency problems that arise when your data pipeline scales.

# Extra Credit
I set up Prometheus monitoring for the cluster.  This means there is built-in monitoring for the each pod & node.
I also installed a redis exporter, which monitors our redis service.
Lastly, I installed Prometheus exporter client on both our Python containers.  This allows us to create custom metrics straight from the Python application.
For example, there's a counter for successful API requests to the Swish API, successful redis insertions, # of games stored in the DB, etc.

This is an easy and effective way to discover and show the status of your services.
It can't quite show the live processing of data, per se, but can give you insights on what and where your data is.

## Extra credit instructions
1. Add ingress to minikube `minikube addons enable ingress`
2. Run `minikube service --namespace=monitoring prometheus`

Your browser should open to the Prometheus dashboard.